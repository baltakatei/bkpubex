#!/bin/bash

# Date: 2020-02-08T22:21Z

# Author: Steven Baltakatei Sandoval

# License: This bash script, 'bkcc', is licensed under GPLv3 or
# later by Steven Baltakatei Sandoval:
#
#    'bkcc', a Creative Commons HTML tag generator.
#    Copyright (C) 2020  Steven Baltakatei Sandoval (baltakatei.com)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    A copy of the GNU General Public License may be found at
#    <https://www.gnu.org/licenses/>.
#
#   The structure of this script was modeled after the following
#   Creative Commons license choosers:
#
#     https://creativecommons.org/choose/
#     https://chooser-beta.creativecommons.org/

# Description: bkcc - A tool that creates a machine-readable (RDFa
# XML) Creative Commons license tag. The tag is generated using
# information prompted from the user and exported to stdout.

# Usage: bkcc [ OPTIONS ]


#==DEFINITIONS==

# regular expression for identifying valid URLs (source: http://regexlib.com/REDetails.aspx?regexp_id=1051 )
regexURL="((http\://|https\://|ftp\://)|(www.))+(([a-zA-Z0-9\.-]+\.[a-zA-Z]{2,4})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(/[a-zA-Z0-9%:/-_\?\.'~]*)?"

# Define 'echoerr' function which outputs text to stderr
    # Note: function copied from https://stackoverflow.com/a/2990533
echoerr() {
    echo "$@" 1>&2;
}

promptAttribute() {
    ###echoerr "Note: RECOMMENDED options promote \"Free Cultural Works\" as defined by creativecommons.org"
    echoerr "======================================================="
    echoerr "Do you want to require attribution for your work? (y/n)"
    echoerr "  y) Yes, anyone using my work must give me appropriate credit (RECOMMENDED)."
    echoerr "  n) No, anyone can use my work even without attribution."
    read m
    case $m in
	y | Y | yes | 1) OPTION_BY="true";;
	n | N | no | 2)
	    echoerr "======================================================="
	    echoerr "I hereby waive all copyright and related or neighboring rights together with all"
	    echoerr "associated claims and causes of action with respect to this work to the extent"
	    echoerr "possible under law according to"
	    echoerr "https://creativecommons.org/publicdomain/zero/1.0/ ."
	    echoerr ""
	    echoerr "I have read and understood the terms and intended legal effect of CC0, and"
	    echoerr "hereby voluntarily elect to apply it to this work (y/n):"
	    read m
	    case $m in
		y | Y | yes | 1) OPTION_BY="false";
				 echoerr "DEBUG: OPTION_BY set to $OPTION_BY"
				 return 1
				 ;;
		n | N | no | 2) echoerr "No license selected. Exiting."; exit 1;;
		*) echoerr "Invalid CC0 release option. Exiting."; exit 1;;
	    esac ;;
	*) echoerr "Invalid attribution requirement option. Exiting."; exit 1;;
    esac
}

promptCommercial() {
    echoerr "======================================================="
    echoerr "Do you want to prohibit others commercial use of your work? (y/n)"
    echoerr "  y) Yes, others cannot use my work for commercial purposes."
    echoerr "  n) No, others can use my work for commercial purposes (RECOMMENDED)."
    read m
    case $m in
	y | Y | yes | 1) OPTION_NC="true"; echoerr "DEBUG: OPTION_NC set to $OPTION_NC";;
	n | N | no | 2) OPTION_NC="false"; echoerr "DEBUG: OPTION_NC set to $OPTION_NC";;
	*) echoerr "Invalid commercial use permission option. Exiting."; exit 1;;
    esac
}

promptDerivatives() {
    echoerr "======================================================="
    echoerr "Do you want to prohibit others from distributing your work if they remix, transform, or build upon it?"
    echoerr "  y) Yes, others can use and share my work only as is, without modifications."
    echoerr "  n) No, others may share modified versions of my work (RECOMMENDED)."
    read m
    case $m in
	y | Y | yes | 1) OPTION_ND="true"; echoerr "DEBUG: OPTION_ND set to $OPTION_ND";;
	n | N | no | 2) OPTION_ND="false"; echoerr "DEBUG: OPTION_ND set to $OPTION_ND";;
	*) echoerr "Invalid derivative use permission option. Exiting."; exit 1;;
    esac
}

promptShareAlike() {
    echoerr "======================================================="
    echoerr "Do you want to prohibit your work under other terms?"
    echoerr "  y) Yes, anyone who changes your work must share their contributions under the same license as the original. (RECOMMENDED)"
    echoerr "  n) No, anyone who changes your work can distribute their contributions under a different license."
    read m
    case $m in
	y | Y | yes | 1) OPTION_SA="true"; echoerr "DEBUG: OPTION_SA set to $OPTION_SA";;
	n | N | no | 2) OPTION_SA="false"; echoerr "DEBUG: OPTION_SA set to $OPTION_SA";;
	*) echoerr "Invalid sharealike requirement option. Exiting."; exit 1;;
    esac
}

promptLicenseDetails() {
    # Usage: promptLicenseDetails
    # Description: Sets LICENSE_TYPE by questioning user.
   
    echoerr "DEBUG: Beginning promptLicenseDetails function."

    # Evaluate BY option.
    # Ask if author wishes to be attributed.
    promptAttribute # set OPTION_BY to true or false.
    if [[ $OPTION_BY == "false" ]]; then
	LICENSE_TYPE="CC 1.0"
	echoerr "DEBUG: LICENSE_TYPE set to $LICENSE_TYPE"
       return 1;
    fi

    # Evaluate NC option.
    # Ask if author permits commercial use of work.
    promptCommercial # set OPTION_NC to true or false.

    # Evaluate ND option.
    # Ask if author permits derivative use of work.
    promptDerivatives # set OPTION_ND to true or false.

    # Evaluate SA option.
    # Ask if author requires derivative works to also carry same license as work.
    if [[ $OPTION_ND = "true" ]]; then 
	OPTION_SA="false" # An SA work cannot be made from an ND work.
    else
	promptShareAlike # set OPTION_SA to true or false.
    fi

    # Check for CC BY
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "false" ]] && \
       [[ $OPTION_ND == "false" ]] && \
       [[ $OPTION_SA == "false" ]]
    then
	LICENSE_TYPE="CC BY 4.0"
	return
    fi

    # Check for CC BY-SA
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "false" ]] && \
       [[ $OPTION_ND == "false" ]] && \
       [[ $OPTION_SA == "true" ]]
    then
	LICENSE_TYPE="CC BY-SA 4.0"
	return
    fi

    # Check for CC BY-ND
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "false" ]] && \
       [[ $OPTION_ND == "true" ]] && \
       [[ $OPTION_SA == "false" ]]
    then
	LICENSE_TYPE="CC BY-ND 4.0"
	return
    fi

    # Check for CC BY-NC
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "true" ]] && \
       [[ $OPTION_ND == "false" ]] && \
       [[ $OPTION_SA == "false" ]]
    then
	LICENSE_TYPE="CC BY-NC 4.0"
	return
    fi
    
    # Check for CC BY-NC-SA
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "true" ]] && \
       [[ $OPTION_ND == "false" ]] && \
       [[ $OPTION_SA == "true" ]]
    then
	LICENSE_TYPE="CC BY-NC-SA 4.0"
	return
    fi
    
    # Check for CC BY-NC-ND
    if [[ $OPTION_BY == "true" ]] && \
       [[ $OPTION_NC == "true" ]] && \
       [[ $OPTION_ND == "true" ]] && \
       [[ $OPTION_SA == "false" ]]
    then
	LICENSE_TYPE="CC BY-NC-ND 4.0"
	return
    fi

    # Error message if none of the above if statements returned out of function.
    echoerr "ERROR: Invalid license specified or LICENSE_TYPE not set."; exit 1;
    
}

selectLicense() {
    # Usage: selectLicense
    # Description: Sets LICENSE_TYPE variable according to a set of user prompts.

    # Prompt license type
    echoerr "Select license type:"
    echoerr "  0) I don't know. Guide me."
    echoerr "  1) CC0 1.0 \"CC0 1.0 Universal\""
    echoerr "  2) CC BY 4.0 \"Attribution 4.0 International\""
    echoerr "  3) CC BY-SA 4.0 \"Attribution-ShareAlike 4.0 International\""
    echoerr "  4) CC BY-ND 4.0 \"Attribution-NoDerivatives 4.0 International\""
    echoerr "  5) CC BY-NC 4.0 \"Attribution-NonCommercial 4.0 International\""
    echoerr "  6) CC BY-NC-SA 4.0 \"Attribution-NonCommercial-ShareAlike 4.0 International\""
    echoerr "  7) CC BY-NC-ND 4.0 \"Attribution-NonCommercial-NoDerivatives 4.0 International\""
    read n
    case $n in
	0) promptLicenseDetails; return;; # Sets LICENSE_TYPE via many questions to user.
	1) LICENSE_TYPE="CC 1.0";;
	2) LICENSE_TYPE="CC BY 4.0";;
	3) LICENSE_TYPE="CC BY-SA 4.0";;
	4) LICENSE_TYPE="CC BY-ND 4.0";;
	5) LICENSE_TYPE="CC BY-NC 4.0";;
	6) LICENSE_TYPE="CC BY-NC-SA 4.0";;
	7) LICENSE_TYPE="CC BY-NC-ND 4.0";;
	*) echoerr "Invalid option selected. Exiting."; exit 1;;
    esac
   
}

#==MAIN PROGRAM==
selectLicense # Sets LICENSE_TYPE via simple prompt.

# Display value of $LICENSE_TYPE
echoerr "======================================================="
echoerr "$LICENSE_TYPE"" license selected."

# Prompt metadata
echoerr "======================================================="
echoerr "Specify metadata (leave blank if not needed):"
read -p "  Title of work [title]:" workTitle
read -p "  Attribute work to [author]:" workAuthor

read -p "  Attribute work to (ex: author's website) [URL]:" workAuthorURL
if [[ -n $workAuthorURL ]]; then
    while [[ ! $workAuthorURL =~ $regexURL ]]; do
	echoerr "WARNING: URL may contain error (ex: does not begin with \"https://\"."
	echoerr "  Preappend \"http://\" to URL? (y/n)"
	read m
	case $m in
	    y | Y | yes | 1) workAuthorURL="http://""$workAuthorURL" ;;
	    n | N | no | 2) read -p "  Attribute work to (ex: author's website) [URL]:" workAuthorURL ;;
	    *) ;;
	esac
    done
fi

read -p "  Source work from which work was derived (leave blank if original work) [URL]:" workSourceURL
if [[ -n $workSourceURL ]]; then
    while [[ ! $workSourceURL =~ $regexURL ]]; do
	echoerr "WARNING: URL may contain error (ex: does not begin with \"https://\."
	echoerr "  Preappend \"http://\" to URL? (y/n)"
	read m
	case $m in
	    y | Y | yes | 1) workSourceURL="http://""$workSourceURL" ;;
	    n | N | no | 2) read -p "  Source work from which work was derived (leave blank if original work) [URL]:" workSourceURL ;;
	    *);;
	esac
    done
fi

read -p "  Webpage where more additional permissions may be requested (OPTIONAL) [URL]:" morePermissionsURL
if [[ -n $morePermissionsURL ]]; then
    while [[ ! $morePermissionsURL =~ $regexURL ]]; do
	echoerr "WARNING: URL may contain error (ex: does not begin with \"https://\."
	echoerr "  Preappend \"http://\" to URL? (y/n)"
	read m
	case $m in
	    y | Y | yes | 1) morePermissionsURL="http://""$morePermissionsURL";;
	    n | N | no | 2) read -p "  Webpage where more additional permissions may be requested (OPTIONAL) [URL]:" morePermissionsURL ;;
	esac
    done
fi

#===CONSTRUCT RDFA TAG===
#====RDFA:Handle workTitle, workAuthor, workAuthorURL====
###echoerr "DEBUG: workTitle is --$workTitle--"
###echoerr "DEBUG: workAuthor is --$workAuthor--"
###echoerr "DEBUG: workAuthorURL is --$workAuthorURL--"
###echoerr "DEBUG: workSourceURL is --$workSourceURL--"
###echoerr "DEBUG: morePermissionsURL is --$morePermissionsURL--"
###echoerr "DEBUG: LICENSE_TYPE is --$LICENSE_TYPE--"

# Exit early if critical parameters not provided
if     [[ -z "$workTitle" ]] || \
       [[ ! -v LICENSE_TYPE ]];
then
    echoerr "ERROR: Problem with workTitle or LICENSE_TYPE."
    exit 1;
fi
  
# Add workTitle to OUTPUT variable
OUTPUT=$OUTPUT"<span xmlns:dct=\"http://purl.org/dc/terms/\" property=\"dct:title\">$workTitle</span>"

# Add workAuthor and/or workAuthorURL to OUTPUT variable (if applicable)
if [[ -v workAuthor ]]; then
    if [[ -v workAuthorURL ]]; then
	# workAuthor set, workAuthorURL set
	OUTPUT=$OUTPUT" by <a xmlns:cc=\"http://creativecommons.org/ns#\" href=\"$workAuthorURL\" property=\"cc:attributionName\" rel=\"cc:attributionURL\">$workAuthor</a>"
    else
	#workAuthor set, workAuthorURL not set
	OUTPUT=$OUTPUT" by <span xmlns:cc=\"http://creativecommons.org/ns#\" property=\"cc:attributionName\">$workAuthor</span>"
   fi
fi

#====RDFA:Handle LICENSE_TYPE====
# Add selected license to OUTPUT variable

# Add boilerplate
OUTPUT=$OUTPUT" is licensed under "

# Add license text
case $LICENSE_TYPE in
    "CC 1.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/publicdomain/zero/1.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC0 1.0</a><a href=\"https://creativecommons.org/publicdomain/zero/1.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-cc0_icon.svg\" /></a>"
	;;
    "CC BY 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY 4.0</a><a href=\"https://creativecommons.org/licenses/by/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /></a>"
	;;
    "CC BY-SA 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by-sa/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY-SA 4.0</a><a href=\"https://creativecommons.org/licenses/by-sa/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-sa_icon.svg\" /></a>"
	;;
    "CC BY-ND 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by-nd/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY-ND 4.0</a><a href=\"https://creativecommons.org/licenses/by-nd/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-nd_icon.svg\" /></a>"
	;;
    "CC BY-NC 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by-nc/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY-NC 4.0</a><a href=\"https://creativecommons.org/licenses/by-nc/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /></a>"
	;;
    "CC BY-NC-SA 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by-nc-sa/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY-NC-SA 4.0</a><a href=\"https://creativecommons.org/licenses/by-nc-sa/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-sa_icon.svg\" /></a>"
	;;
    "CC BY-NC-ND 4.0")
	OUTPUT=$OUTPUT"<a href=\"https://creativecommons.org/licenses/by-nc-nd/4.0/\" target=\"_blank\" rel=\"license noopener noreferrer\" style=\"display: inline-block;\">CC BY-NC-ND 4.0</a><a href=\"https://creativecommons.org/licenses/by-nc-nd/4.0/\"><img style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img  style=\"height:22px!important;margin-left: 3px;vertical-align:text-bottom;opacity:0.7;\" src=\"https://search.creativecommons.org/static/img/cc-nd_icon.svg\" /></a>"
	;;
    *) echoerr "Error."; exit 1;;
esac
# End sentence.
OUTPUT=$OUTPUT"."


#====RDFA:Handle workSourceURL, morePermissionsURL (if applicable)====
# Add workSourceURL to OUTPUT (if applicable)
if [[ -n "$workSourceURL" ]]; then
    OUTPUT=$OUTPUT"<br />$workTitle is based on a work at <a xmlns:dct=\"http://purl.org/dc/terms/\" href=\"$workSourceURL\" rel=\"dct:source\">$workSourceURL</a>"
    # End sentence.
    OUTPUT=$OUTPUT"."
fi

# Add morePermissionsURL to OUTPUT (if applicable)
if [[ -n "$morePermissionsURL" ]]; then
    OUTPUT=$OUTPUT"<br />Permissions beyond the scope of this license may be available at <a xmlns:cc=\"http://creativecommons.org/ns#\" href=\"$morePermissionsURL\" rel=\"cc:morePermissions\">$morePermissionsURL</a>"
    #End sentence
    OUTPUT=$OUTPUT"."
fi



#====Deliver output====
# Send OUTPUT to stdout.
echo $OUTPUT 
